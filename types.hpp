#pragma once

#include <cstddef>
#include <cstdint>
#include <utility>

namespace dk::types {

using u8 = std::uint8_t;
using u16 = std::uint16_t;
using u32 = std::uint32_t;
using u64 = std::uint64_t;
using i8 = std::int8_t;
using i16 = std::int16_t;
using i32 = std::int32_t;
using i64 = std::int64_t;

using usize = std::size_t;
using isize = std::ptrdiff_t;

using f32 = float;
using f64 = double;

template <typename T, typename Parameter>
class NamedType {
 public:
  constexpr explicit NamedType(T const& val) : value(val) {}
  constexpr explicit NamedType(T&& val) : value(std::move(val)) {}
  constexpr auto get() noexcept -> T& { return value; }
  constexpr auto get() const noexcept -> T const& { return value; }

 private:
  T value;
};

}  // namespace dk::types
