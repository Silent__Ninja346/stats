#include <numeric>
#include <string>
#include <string_view>
#include <tl/optional.hpp>

#include "stats.hpp"
#include "types.hpp"

namespace dk::fraction {

using namespace std::string_view_literals;

using dk::types::f64;
using dk::types::i64;
using dk::types::NamedType;
using std::signed_integral;
using std::string_view;
using tl::optional;

using Numerator = NamedType<i64, struct NumeratorParameter>;
using Denominator = NamedType<i64, struct DenominatorParameter>;

template <signed_integral T = i64>
class Fraction {
  T numerator;
  T denominator;

  // this is unchecked, so it should be private so the user can't access it
  constexpr explicit Fraction(const Numerator num, const Denominator den) noexcept
      : numerator(num.get()), denominator(den.get()) {}

 public:
  constexpr explicit Fraction() noexcept : numerator(0), denominator(1) {}
  constexpr explicit Fraction(const signed_integral auto val) noexcept
      : numerator(val), denominator(1) {}

  // alternative to the two arg constructor so the user knows they're doing this unsafely
  constexpr static auto make_fraction_unchecked(const Numerator num, const Denominator den) noexcept
      -> Fraction {
    if (num.get() == 0) {
      return Fraction();
    }
    return Fraction(num, den);
  }
  constexpr static auto make_fraction(const Numerator num, const Denominator den) noexcept
      -> optional<Fraction> {
    if (den.get() == 0) {
      return tl::nullopt;
    }
    return make_fraction_unchecked(num, den);
  }

  constexpr auto to_reduced() const noexcept -> Fraction {
    const auto gcd = std::gcd(this->numerator, this->denominator);
    return Fraction(Numerator(this->numerator / gcd), Denominator(this->denominator / gcd));
  }

  constexpr auto operator+=(const Fraction<T>& rhs) noexcept -> Fraction& {
    this->numerator = this->numerator * rhs.denominator + rhs.numerator * this->denominator;
    this->denominator *= rhs.denominator;
    return *this;
  }

  constexpr auto operator-=(const Fraction<T>& rhs) noexcept -> Fraction& {
    this->numerator = this->numerator * rhs.denominator - rhs.numerator * this->denominator;
    this->denominator *= rhs.denominator;
    return *this;
  }

  constexpr auto operator*=(const Fraction<T>& rhs) noexcept -> Fraction& {
    this->numerator *= rhs.numerator;
    this->denominator *= rhs.denominator;
    return *this;
  }

  constexpr auto operator/=(const Fraction<T>& rhs) noexcept -> Fraction& {
    this->numerator *= rhs.denominator;
    this->denominator *= rhs.numerator;
    return *this;
  }

  constexpr explicit operator f64() const noexcept {
    return static_cast<f64>(numerator) / denominator;
  }

  constexpr auto operator==(const Fraction<T>& rhs) const noexcept -> bool {
    if (this->denominator == rhs.denominator) {
      return this->numerator == rhs.numerator;
    }
    return this->numerator * rhs.denominator == rhs.numerator * this->denominator;
  }

  constexpr auto operator-() const noexcept -> Fraction {
    return Fraction(-this->numerator, this->denominator);
  }
};

template <signed_integral T>
constexpr auto operator+(Fraction<T> lhs, const Fraction<T>& rhs) noexcept -> Fraction<T> {
  return lhs += rhs;
}

template <signed_integral T>
constexpr auto operator-(Fraction<T> lhs, const Fraction<T>& rhs) noexcept -> Fraction<T> {
  return lhs -= rhs;
}

template <signed_integral T>
constexpr auto operator*(Fraction<T> lhs, const Fraction<T>& rhs) noexcept -> Fraction<T> {
  return lhs *= rhs;
}

template <signed_integral T>
constexpr auto operator/(Fraction<T> lhs, const Fraction<T>& rhs) noexcept -> Fraction<T> {
  return lhs /= rhs;
}

static_assert(stats::concepts::SupportedStatsType<Fraction<i64>>);

}  // namespace dk::fraction
