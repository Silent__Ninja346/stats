#include <algorithm>
#include <boost/ut.hpp>
#include <exception>
#include <initializer_list>
#include <iterator>
#include <string_view>
#include <tl/expected.hpp>
#include <vector>

#include "fraction.hpp"
#include "stats.hpp"
#include "types.hpp"

auto main() -> int {
  using namespace std::string_view_literals;
  using namespace boost::ut;
  using namespace boost::ut::bdd;

  using dk::fraction::Denominator;
  using dk::fraction::Fraction;
  using dk::fraction::Numerator;
  using dk::stats::geometric_mean;
  using dk::types::f64;
  using dk::types::u64;
  using std::begin;
  using std::end;
  using std::vector;

  feature("geomteric_mean") = [] {
    scenario("Small std::vector of u64") = [] {
      given("I have a vector of 10 u64") = [] {
        const vector<u64> vec = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        expect((!vec.empty() and vec.size() == 10_ul) >> fatal);

        when("I call geometric_mean") = [=] {
          const auto x = geometric_mean(vec).value();

          then("The geometric_mean is 4.5287") = [=] { expect(x == 4.5287_d); };
        };
      };
    };

    scenario("Empty std::vector of u64") = [] {
      given("I have an empty vector of u64") = [] {
        const vector<u64> vec = {};
        expect(vec.empty() >> fatal);

        when("I call geometric_mean") = [=] {
          const auto x = geometric_mean(vec).error();

          then("I recieve an unexpected") = [=] { expect(x == "data has a size of zero"sv); };
        };
      };
    };

    scenario("Small std::vector of Fraction") = [] {
      given("I have a vector of 10 Fraction") = [] {
        const vector<Fraction<>> vec = {Fraction(1), Fraction(2), Fraction(3), Fraction(4),
                                        Fraction(5), Fraction(6), Fraction(7), Fraction(8),
                                        Fraction(9), Fraction(10)};
        expect((!vec.empty() and vec.size() == 10_ul) >> fatal);

        when("I call geometric_mean") = [=] {
          const auto x = geometric_mean(vec).value();

          then("The geometric_mean is 4.5287") = [=] {
            constexpr auto y = 4.5287_d;
            expect(x == y);
          };
        };
      };
    };
  };
}
