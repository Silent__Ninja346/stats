# stats.hpp

> A header only library for basic statistics operations.

<!-- ## TODO -->
<!---->
<!-- - [ ] Update this README -->
<!-- - [ ] Switch to using std::variant for error handling -->
<!-- - [ ] Ensure tests cover everything they need to (code coverage time?) -->
<!-- - [ ] Add doctest.h via CMake to auto fetch the latest version -->
<!-- - [ ] For functions which don't need a double result, return the same datatype -->
<!--       as the container -->
<!-- - [ ] For functions which sort data, use a vec of the same datatype? -->
<!--   - Maybe use an array instead as well? -->
<!-- - [ ] Redo documentation for all functions -->
<!-- - [x] Create a justfile for running common commands -->
<!-- - [ ] Return arrays in places where the size of the returned container is known? -->
<!-- - [ ] Mark functions which could result in a loss of data -->
<!-- - [ ] A whole wack ton of other stuff :( -->

## General Info

This is a C++ 17 header only library providing functions for calculating basic
statistics of data. This is not meant to be a replacement to massive machine
learning libraries like dlib or CUDA, or even for more complicated statistics
with distributions and whatnot. It's just something small to include with
projects that need basic statistics.

All functions expect the user to pass them either STL compatible iterators or a
container that provides STL compatible iterators.

## Technologies

- [meson](https://mesonbuild.com/): My build system of choice. Used for building
  and running tests, getting dependencies, and installing on the user's system
  if desired.

- [doctest.h](https://github.com/onqtam/doctest): Single header library used for
  writing unit tests.

<!-- - [just](https://github.com/casey/just): General purpose command runner -->
<!-- - [lcov](https://github.com/linux-test-project/lcov): Tool for visualizing code -->
<!--   coverage with GNU gcov extensions -->
<!-- - [doxygen](https://www.doxygen.nl/index.html): System for generating -->
<!--   documentation of the library. -->

## Use

Single header libraries are meant to be super easy to use, just drop `stats.hpp`
into your project, and include it.

## Features

## Inspiration

This project is heavily inspired by the built-in
[statistics](https://docs.python.org/3/library/statistics.html) library from
Python 3.4 and onward. Ideally this project will reach feature parity with
Python's library, while being as easy to use from within a C++ project.
